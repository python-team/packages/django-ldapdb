Django>=2.2
python-ldap>=3.0

[dev]
check-manifest
flake8
isort>=5.0.0
tox
factory_boy
volatildap>=1.1.0
wheel
zest.releaser[recommended]
