From: Bryan Davis <bd808@wikimedia.org>
Date: Wed, 17 Jul 2019 21:00:54 -0600
Subject: Use _base_manager instead of objects when modifying model

Guard against issues with a non-empty default QuerySet returned by
a Model's Manager instance by calling the Model's _base_manager when
doing lookups by dn. LDAP lookups by dn must be done by setting the dn
as the base of the LDAP query.

The current method for determining if a dn lookup is being requested
relies on inspecting the generated django.db.models.sql.query.Query and
looking for a 'dn' target column in the first where clause statement. If
a Model uses a custom Manager to append a where clause statement to all
QuerySets, this logic is broken. Using _base_manager instead of objects
as the Manager for constructing explicit dn lookups avoids this problem.
This behavior also matches the Django documentation and internals for
related object access, so it is likely to continue to work for the
foreseeable future.

Fixes #196

Forwarded: not-needed
---
 examples/models.py    | 13 +++++++++++++
 examples/tests.py     | 43 ++++++++++++++++++++++++++++++++++++++++++-
 ldapdb/models/base.py |  2 +-
 3 files changed, 56 insertions(+), 2 deletions(-)

diff --git a/examples/models.py b/examples/models.py
index a1800fe..d241989 100644
--- a/examples/models.py
+++ b/examples/models.py
@@ -2,6 +2,8 @@
 # This software is distributed under the two-clause BSD license.
 # Copyright (c) The django-ldapdb project
 
+from django.db.models import Manager
+
 import ldapdb.models
 from ldapdb.models import fields
 
@@ -98,3 +100,14 @@ class AbstractGroup(ldapdb.models.Model):
 
 class ConcreteGroup(AbstractGroup):
     base_dn = "ou=groups,dc=example,dc=org"
+
+
+class FooNamePrefixManager(Manager):
+    def get_queryset(self):
+        return super(FooNamePrefixManager, self).get_queryset().filter(
+            name__startswith='foo')
+
+
+class FooGroup(AbstractGroup):
+    base_dn = "ou=groups,dc=example,dc=org"
+    objects = FooNamePrefixManager()
diff --git a/examples/tests.py b/examples/tests.py
index 5ad2d84..5d6e442 100644
--- a/examples/tests.py
+++ b/examples/tests.py
@@ -18,7 +18,8 @@ from django.db.models import Count, Q
 from django.test import TestCase
 from django.utils import timezone
 
-from examples.models import ConcreteGroup, LdapGroup, LdapMultiPKRoom, LdapUser
+from examples.models import (ConcreteGroup, FooGroup, LdapGroup,
+                             LdapMultiPKRoom, LdapUser)
 from ldapdb.backends.ldap.compiler import SQLCompiler, query_as_ldap
 
 groups = ('ou=groups,dc=example,dc=org', {
@@ -890,3 +891,43 @@ class AdminTestCase(BaseTestCase):
         response = self.client.post('/admin/examples/ldapuser/foouser/delete/',
                                     {'yes': 'post'})
         self.assertRedirects(response, '/admin/examples/ldapuser/')
+
+
+class FooGroupTestCase(BaseTestCase):
+    directory = dict([groups, foogroup, bargroup, wizgroup, people, foouser])
+
+    def as_ldap_query(self, qs):
+        connection = connections['ldap']
+        compiler = SQLCompiler(
+            query=qs.query,
+            connection=connection,
+            using=None,
+        )
+        return query_as_ldap(qs.query, compiler, connection)
+
+    def test_count_all(self):
+        qs = FooGroup.objects.all()
+        lq = self.as_ldap_query(qs)
+        self.assertEqual(lq.base, groups[0])
+        self.assertEqual(lq.filterstr, '(&(objectClass=posixGroup)(cn=foo*))')
+        self.assertEqual(qs.count(), 1)
+
+    def test_base_manager_dn(self):
+        dn = foogroup[0]
+        qs = FooGroup._base_manager.using(None).filter(dn=dn)
+        lq = self.as_ldap_query(qs)
+        self.assertEqual(lq.base, dn)
+        self.assertEqual(lq.filterstr, '(&(objectClass=posixGroup))')
+        self.assertEqual(qs.count(), 1)
+
+    def test_update_group(self):
+        g = FooGroup.objects.get(name='foogroup')
+        g.gid = 1002
+        g.usernames = ['foouser2', u'baruseeer2']
+        g.save()
+
+        # check group was updated
+        new = FooGroup.objects.get(name='foogroup')
+        self.assertEqual(new.name, 'foogroup')
+        self.assertEqual(new.gid, 1002)
+        self.assertCountEqual(new.usernames, ['foouser2', u'baruseeer2'])
diff --git a/ldapdb/models/base.py b/ldapdb/models/base.py
index 6ac2827..e4d9e15 100644
--- a/ldapdb/models/base.py
+++ b/ldapdb/models/base.py
@@ -89,7 +89,7 @@ class Model(django.db.models.base.Model):
         if create:
             old = None
         else:
-            old = cls.objects.using(using).get(dn=self._saved_dn)
+            old = cls._base_manager.using(using).get(dn=self._saved_dn)
         changes = {
             field.db_column: (
                 None if old is None else get_field_value(field, old),
